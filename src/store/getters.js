export default {
  cartCount (state) {
    return state.cart.reduce((total, item) => {
      total += item.count
      return total
    }, 0)
  },
  totalMoney (state) {
    let sum = 0
    state.cart.forEach(item => {
      sum += item.price * item.count
    })
    return sum.toFixed(2)
  },

}
