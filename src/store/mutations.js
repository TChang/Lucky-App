export default {
  addToCart (state, id) {
    console.log(id, 222)
    const isInCart = state.cart.some(item => item.id === id.id)
    console.log(7098, isInCart)
    if (isInCart) {
      state.cart = state.cart.map((item) => {
        const newItem = Object.assign({}, item)
        if (newItem.id === id.id) {
          newItem.count += 1
        }
        return newItem
      })
    } else {
      state.cart.push({
        count: 1,
        ...id
      })
    }
    window.localStorage.setItem('ca-cart', JSON.stringify(state.cart))
  },
  saveUserinfo (state, vals) {
    state.userinfo.phone = vals
  },  
  saveToken (state, vals) {
    state.token = vals
  },
  getuserInfo(state,vals){
    state.detail=vals
  },
  paySuc(state,vals){
    
    let  userBanlance= parseFloat(state.detail.filter(item=>{
              return item.phone==vals.phone
           })[0].balance);
    let  goodMoney=vals.allMoney;

           if(userBanlance < goodMoney){
             state.payMessage=false  
           }else{
              userBanlance=userBanlance-goodMoney;
              state.detail.filter(item=>{
                return item.phone==vals.phone
            })[0].balance=userBanlance.toFixed(2);
              console.log(userBanlance)
             state.payMessage=true
          }
    },
  resetPhone(state){
    state.userinfo.phone=''
  },
  
  changeCartCountByIdAndType (state, payload) {
    state.cart = state.cart.map((item) => {
      const newItem = Object.assign({}, item)
      // console.log(111, newItem)
      // console.log(payload)
      if (newItem.id === payload.id) {
      // console.log(1, payload.type)
        if (payload.type === 'add') {
          // console.log(payload.type)
          newItem.count += 1
        } else {
          newItem.count -= 1
          if (newItem.count <= 1) {
            newItem.count = 1
          }
        }
      }
      return newItem
    })
    window.localStorage.setItem('ca-cart', JSON.stringify(state.cart))
  },
  toggleTabbar (state, show = true) {
    state.isShowTabbar = show
  }
}
