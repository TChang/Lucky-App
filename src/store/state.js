export default {
  count: 100,
  cart: JSON.parse(window.localStorage.getItem('ca-cart')) || [],
  isLogin:false,
  transtoken:'',
  userinfo:{
    phone:'',
    alias:'',
    token:''
  },
 detail:[],
 isShowTabbar: true,
 payMessage:false
}
