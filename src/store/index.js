import Vue from 'vue'
import VueX from 'vuex'
import state from './state'
import mutations from './mutations'
import getters from './getters'
Vue.use(VueX)

const store = new VueX.Store({
  strict: true,
  state,
  getters,
  mutations
})

export default store
