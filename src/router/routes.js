
const Home = () => import('@/views/Home')
const Menus = () => import('@/views/Menus')
const Cart = () => import('@/views/Cart')
const Mine = () => import('@/views/Mine')
const Detail = () => import('@/views/Detail')
const Login = () => import('@/views/Login')

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    name: 'home',
    icon: 'home',
    component: Home,
    title: '首页'
  },
  {
    path: '/menus',
    name: 'menus',
    icon: 'menu',
    component: Menus,
    title: '菜单'
  },
  {
    path: '/cart',
    name: 'cart',
    icon: 'cart',
    component: Cart,
    title: '购物车'
  },
  {
    path: '/mine',
    name: 'mine',
    icon: 'mine',
    component: Mine,
    title: '我的'
  },
  {
    path: '/detail',
    name: 'detail',
    component: Detail
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  }
]

export default routes
