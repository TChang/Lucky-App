import  Coupon  from '@/views/Mine_Func/Coupon'
import  Ticket  from '@/views/Mine_Func/Ticket'
import  Help  from '@/views/Mine_Func/Help'
import  Personal  from '@/views/Mine_Func/Personal'
import  Wallet  from '@/views/Mine_Func/Wallet'
import { deflate } from 'zlib';

const component={
    Coupon,
    Ticket,
    Help,
    Personal,
    Wallet
}

export  default component;