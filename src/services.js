import axios from 'axios'
import {Indicator} from 'mint-ui'

const isDev = process.env.NODE_ENV === 'development'
const ajax = axios.create({
  baseURL: isDev ? 'http://rap2api.taobao.org/app/mock' : 'http://rap2api.taobao.org/app/mock'

})

ajax.interceptors.request.use((config) => {
  Indicator.open({
    text: '加载中...',
    spinnerType: 'fading-circle'
  })
  return config
})
// ajax.interceptors.response.use((resp) => {
//   let ret = null
//   if (resp.data.code === 200) {
//     ret = resp.data.data
//   } else {
//     ret = resp.data.errMessage
//   }
//   return ret
// })

ajax.interceptors.response.use((resp) => {
  let ret = null
  
  if (resp.data.code === 200) {
    
    ret = Promise.resolve(resp.data.data)
  } else {
    ret = Promise.reject(resp.data.errMessage)
  }
  Indicator.close()
  return ret
})

export default ajax

export const getHomeSwiper = () => ajax.get('/85202/api/v2/homeSwiper')

//   zconst now = new Date().getTime()
// export const getGoodsList = (config) => {
//   const {
//     page,
//     count
//   } = config
//   return ajax.get(`/mz/v4/api/film/now-playing?_t=${now}&page=${page}&count=${count}`)
// }

export const getGoodsList = () => ajax.get('/85202/api/v2/page')
export const getDetail = () => ajax.get('/85202/api/v2/cart')
// export const getAllGoods = () => getGoodsList({ page: 1, count: 10000 })

//请求验证token
export const getToken=(obj)=>ajax.post('85193/api/userCheck',obj)

//请求用户详细信息

export const getUserDetail=()=>ajax.get('85193/api/userDetail')
